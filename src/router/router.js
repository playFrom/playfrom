import vue from "vue"
import router from "vue-router"
import Login from "../view/login/login.vue"
import Resigin from "../view/login/resigin.vue"
import Space from "../view/index/space.vue"
import Lose_efficacy from "../view/login/lose_efficacy.vue"
import Resourse from "../view/index/resourse.vue"
import Bean from "../view/index/bean.vue"
import AddResoure from "../view/index/addResoure.vue"
import Opinion from "../view/opinion/opinion.vue"
import Helper from "../view/opinion/helper.vue"
import User from "../view/user/user.vue"
import AddUser from "../view/user/AddUser.vue"
import Brand from "../view/brandlist/brandlist.vue"
import Building from "../view/brandlist/buildinglist.vue"
import Feedback from "../view/brandlist/feedback.vue"
import Error from "../view/helper/error.vue"
import Findpassword from "../view/login/findpassword.vue"
import Updata from "../view/index/updata.vue"
import Pull from "../view/index/pull.vue"
vue.use(router)
let routes=[
     {
        path:'/',
		redirect:{
			name:"login"
		}
    },
    {
        "name":"login",
        "path":"/login",
        "component":Login
    },
    {
        "name":"resigin",
        "path":"/resigin",
        "component":Resigin
    },
    {
        "name":"Findpassword",
        "path":"/Findpassword",
        "component":Findpassword
    },
    {
        "name":"space",
        "path":"/space",
        "component":Space
    },
    {
        "name":"resourse",
        "path":"/resourse",
        "component":Resourse
    },
    {
        "name":"Lose_efficacy",
        "path":"/Lose_efficacy",
        "component":Lose_efficacy
    },
    {
        "name":"updata",
        "path":"/updata",
        "component":Updata
    },
    {
        "name":"pull",
        "path":"/pull",
        "component":Pull
    },
    {
        "name":"addResoure",
        "path":"/addResoure",
        "component":AddResoure
    },
    {
        "name":"bean",
        "path":"/bean",
        "component":Bean
    },
    {
        "name":"opinion",
        "path":"/opinion",
        "component":Opinion
    },
    {
        "name":"user",
        "path":"/user",
        "component":User
    },
    {
        "name":"helper",
        "path":"/helper",
        "component":Helper
    },
    {
        "name":"addUser",
        "path":"/addUser",
        "component":AddUser
    },
    {
        "name":"brandlist",
        "path":"/brandlist",
        "component":Brand
    },
    {
        "name":"buildinglist",
        "path":"/buildinglist",
        "component":Building
    },
    {
        "name":"feedback",
        "path":"/feedback",
        "component":Feedback
    },
     {
        "name":"error",
        "path":"/error",
        "component":Error
    }
]
export default new router({
    routes,
})