import Vue from 'vue'
import App from './App.vue'
import router from "./router/router.js"
import iview from "iview"
Vue.use(iview)
new Vue({
  el: '#app',
  router,
  render: h => h(App)
})
